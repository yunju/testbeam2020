#ifndef GetHist_h
#define GetHist_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <map>

typedef struct{
  vector<Float_t> x_biasv;
  vector<Float_t> y_variable;
  vector<Float_t> y_error;
  TGraphErrors *graph;
} HistContent;

class GetHist {
public:
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Declaration of leaf types
  Int_t           FEI4x;
  Int_t           FEI4y;
  Int_t           runNumber;
  Int_t           eventNumber;
  Int_t           cycleNumber;
  Float_t         eventTime;
  Int_t           nChannel;
  Float_t         pulseHeightNoise[8];   //[nChannel]
  Float_t         pulseHeight[8];   //[nChannel]
  Float_t         timeCFD[8];   //[nChannel]
  Float_t         timeCFD10[8];   //[nChannel]
  Float_t         timeCFD20[8];   //[nChannel]
  Float_t         timeCFD30[8];   //[nChannel]
  Float_t         timeCFD40[8];   //[nChannel]
  Float_t         timeCFD50[8];   //[nChannel]
  Float_t         timeCFD60[8];   //[nChannel]
  Float_t         timeCFD70[8];   //[nChannel]
  Float_t         timeCFD80[8];   //[nChannel]
  Float_t         timeCFD90[8];   //[nChannel]
  Float_t         timeZCD[8];   //[nChannel]
  // Float_t         timeZCD20[8];   //[nChannel]
  // Float_t         timeZCD50[8];   //[nChannel]
  Float_t         timeCTD[8];   //[nChannel]
  Float_t         timeAtMax[8];   //[nChannel]
  Float_t         pedestal[8];   //[nChannel]
  Float_t         noise[8];   //[nChannel]
  // Float_t         noiseZCD[8];   //[nChannel]
  Float_t         charge[8];   //[nChannel]
  Float_t         jitter[8];   //[nChannel]
  Float_t         jitterCFD10[8];   //[nChannel]
  Float_t         jitterCFD20[8];   //[nChannel]
  Float_t         jitterCFD30[8];   //[nChannel]
  Float_t         jitterCFD40[8];   //[nChannel]
  Float_t         jitterCFD50[8];   //[nChannel]
  Float_t         jitterCFD60[8];   //[nChannel]
  Float_t         jitterCFD70[8];   //[nChannel]
  Float_t         jitterCFD80[8];   //[nChannel]
  Float_t         jitterCFD90[8];   //[nChannel]
  // Float_t         jitterCTD[8];   //[nChannel]
  // Float_t         riseTime1090[8];   //[nChannel]
  Float_t         derivative[8];   //[nChannel]
  Float_t         derivativeCFD10[8];   //[nChannel]
  Float_t         derivativeCFD20[8];   //[nChannel]
  Float_t         derivativeCFD30[8];   //[nChannel]
  Float_t         derivativeCFD40[8];   //[nChannel]
  Float_t         derivativeCFD50[8];   //[nChannel]
  Float_t         derivativeCFD60[8];   //[nChannel]
  Float_t         derivativeCFD70[8];   //[nChannel]
  Float_t         derivativeCFD80[8];   //[nChannel]
  Float_t         derivativeCFD90[8];   //[nChannel]
  // Float_t         derivativeZCD[8];   //[nChannel]
  // Float_t         derivativeCTD[8];   //[nChannel]
  Float_t         width[8];   //[nChannel]
  Float_t         TOT[8];   //[nChannel]

  // List of branches
  TBranch        *b_FEI4x;   //!
  TBranch        *b_FEI4y;   //!
  TBranch        *b_runNumber;   //!
  TBranch        *b_eventNumber;   //!
  TBranch        *b_cycleNumber;   //!
  TBranch        *b_eventTime;   //!
  TBranch        *b_nChannel;   //!
  TBranch        *b_pulseHeightNoise;   //!
  TBranch        *b_pulseHeight;   //!
  TBranch        *b_timeCFD;   //!
  TBranch        *b_timeCFD10;   //!
  TBranch        *b_timeCFD20;   //!
  TBranch        *b_timeCFD30;   //!
  TBranch        *b_timeCFD40;   //!
  TBranch        *b_timeCFD50;   //!
  TBranch        *b_timeCFD60;   //!
  TBranch        *b_timeCFD70;   //!
  TBranch        *b_timeCFD80;   //!
  TBranch        *b_timeCFD90;   //!
  TBranch        *b_timeZCD;   //!
  // TBranch        *b_timeZCD20;   //!
  // TBranch        *b_timeZCD50;   //!
  TBranch        *b_timeCTD;   //!
  TBranch        *b_timeAtMax;   //!
  TBranch        *b_pedestal;   //!
  TBranch        *b_noise;   //!
  // TBranch        *b_noiseZCD;   //!
  TBranch        *b_charge;   //!
  TBranch        *b_jitter;   //!
  TBranch        *b_jitterCFD10;   //!
  TBranch        *b_jitterCFD20;   //!
  TBranch        *b_jitterCFD30;   //!
  TBranch        *b_jitterCFD40;   //!
  TBranch        *b_jitterCFD50;   //!
  TBranch        *b_jitterCFD60;   //!
  TBranch        *b_jitterCFD70;   //!
  TBranch        *b_jitterCFD80;   //!
  TBranch        *b_jitterCFD90;   //!
  // TBranch        *b_jitterCTD;   //!
  // TBranch        *b_riseTime1090;   //!
  TBranch        *b_derivative;   //!
  TBranch        *b_derivativeCFD10;   //!
  TBranch        *b_derivativeCFD20;   //!
  TBranch        *b_derivativeCFD30;   //!
  TBranch        *b_derivativeCFD40;   //!
  TBranch        *b_derivativeCFD50;   //!
  TBranch        *b_derivativeCFD60;   //!
  TBranch        *b_derivativeCFD70;   //!
  TBranch        *b_derivativeCFD80;   //!
  TBranch        *b_derivativeCFD90;   //!
  // TBranch        *b_derivativeCTD;   //!
  // TBranch        *b_derivativeZCD;   //!
  TBranch        *b_width;   //!
  TBranch        *b_TOT;   //!

  GetHist(string arg_batch="101", TTree *tree=0);
  virtual ~GetHist();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);

  // Additional Variables
  string use_batch;
  typedef struct{
    string batch;
    string channel;
    string sensor;
    string biasV;
  } Point;
  vector<Point> v_point;
  static map< string, map<string, map<string, TH1D*> > > h_prefit; // sensor -> biasv -> var -> hist
  static map< string, map<string, map<string, TH1D*> > > h_dT; // batch -> frac(%) -> combination -> hist
  static TFile *out;

  // Additional Functons
  void GetBatchInfo();
  void PrintBatch();
  void DrawHist();
  void SaveHist();
};

#endif

#ifdef GetHist_cxx
GetHist::GetHist(string arg_batch, TTree *tree) : fChain(0), use_batch(arg_batch)
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.

  if (tree == 0) {

      TChain * chain = new TChain("tree","");

      ifstream inFile("batch_in_CFDfrac.txt");
      string line, tmp, dir, batch;
      bool is_this_batch = kFALSE;

      // Get the path of the directory of ntuples
      getline(inFile, line);
      istringstream ss(line);
      ss >> dir;
      cout << "------------------------------------------------------------------------------" << endl;
      cout << setw(48) << "Adding Trees from Batch " << use_batch << endl;
      cout << "------------------------------------------------------------------------------" << endl;
      cout << "- use dir:  " << dir << endl;
      cout << "- use batch:  " << use_batch << endl;
      cout << "- use files:  " << endl;

      // Include the files
      while ( getline(inFile, line) ) {
        istringstream ss(line);
        ss >> tmp;
        if (tmp == "batch") {
          ss >> batch;
          if (batch == use_batch) is_this_batch = kTRUE;
          else is_this_batch = kFALSE;
        }
        else { // tmp != "batch" -> add files
          if (is_this_batch) {
            chain->Add( (dir+"/data_"+tmp+".dat.root/tree").c_str() );
            cout << (dir+"/data_"+tmp+".dat.root/tree").c_str() << endl;
          }
          else continue;
        }
      }
      inFile.close();
      tree = chain;
  }
  Init(tree);
}

GetHist::~GetHist()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t GetHist::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}

Long64_t GetHist::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
  }
  return centry;
}

void GetHist::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("FEI4x", &FEI4x, &b_FEI4x);
  fChain->SetBranchAddress("FEI4y", &FEI4y, &b_FEI4y);
  fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
  fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
  fChain->SetBranchAddress("cycleNumber", &cycleNumber, &b_cycleNumber);
  fChain->SetBranchAddress("eventTime", &eventTime, &b_eventTime);
  fChain->SetBranchAddress("nChannel", &nChannel, &b_nChannel);
  fChain->SetBranchAddress("pulseHeightNoise", pulseHeightNoise, &b_pulseHeightNoise);
  fChain->SetBranchAddress("pulseHeight", pulseHeight, &b_pulseHeight);
  fChain->SetBranchAddress("timeCFD", timeCFD, &b_timeCFD);
  fChain->SetBranchAddress("timeCFD10", timeCFD10, &b_timeCFD10);
  fChain->SetBranchAddress("timeCFD20", timeCFD20, &b_timeCFD20);
  fChain->SetBranchAddress("timeCFD30", timeCFD30, &b_timeCFD30);
  fChain->SetBranchAddress("timeCFD40", timeCFD40, &b_timeCFD40);
  fChain->SetBranchAddress("timeCFD50", timeCFD50, &b_timeCFD50);
  fChain->SetBranchAddress("timeCFD60", timeCFD60, &b_timeCFD60);
  fChain->SetBranchAddress("timeCFD70", timeCFD70, &b_timeCFD70);
  fChain->SetBranchAddress("timeCFD80", timeCFD80, &b_timeCFD80);
  fChain->SetBranchAddress("timeCFD90", timeCFD90, &b_timeCFD90);
  fChain->SetBranchAddress("timeZCD", timeZCD, &b_timeZCD);
  // fChain->SetBranchAddress("timeZCD20", timeZCD20, &b_timeZCD20);
  // fChain->SetBranchAddress("timeZCD50", timeZCD50, &b_timeZCD50);
  fChain->SetBranchAddress("timeCTD", timeCTD, &b_timeCTD);
  fChain->SetBranchAddress("timeAtMax", timeAtMax, &b_timeAtMax);
  fChain->SetBranchAddress("pedestal", pedestal, &b_pedestal);
  fChain->SetBranchAddress("noise", noise, &b_noise);
  // fChain->SetBranchAddress("noiseZCD", noiseZCD, &b_noiseZCD);
  fChain->SetBranchAddress("charge", charge, &b_charge);
  fChain->SetBranchAddress("jitter", jitter, &b_jitter);
  fChain->SetBranchAddress("jitterCFD10", jitterCFD10, &b_jitterCFD10);
  fChain->SetBranchAddress("jitterCFD20", jitterCFD20, &b_jitterCFD20);
  fChain->SetBranchAddress("jitterCFD30", jitterCFD30, &b_jitterCFD30);
  fChain->SetBranchAddress("jitterCFD40", jitterCFD40, &b_jitterCFD40);
  fChain->SetBranchAddress("jitterCFD50", jitterCFD50, &b_jitterCFD50);
  fChain->SetBranchAddress("jitterCFD60", jitterCFD60, &b_jitterCFD60);
  fChain->SetBranchAddress("jitterCFD70", jitterCFD70, &b_jitterCFD70);
  fChain->SetBranchAddress("jitterCFD80", jitterCFD80, &b_jitterCFD80);
  fChain->SetBranchAddress("jitterCFD90", jitterCFD90, &b_jitterCFD90);
  // fChain->SetBranchAddress("jitterCTD", jitterCTD, &b_jitterCTD);
  // fChain->SetBranchAddress("riseTime1090", riseTime1090, &b_riseTime1090);
  fChain->SetBranchAddress("derivative", derivative, &b_derivative);
  fChain->SetBranchAddress("derivativeCFD10", derivativeCFD10, &b_derivativeCFD10);
  fChain->SetBranchAddress("derivativeCFD20", derivativeCFD20, &b_derivativeCFD20);
  fChain->SetBranchAddress("derivativeCFD30", derivativeCFD30, &b_derivativeCFD30);
  fChain->SetBranchAddress("derivativeCFD40", derivativeCFD40, &b_derivativeCFD40);
  fChain->SetBranchAddress("derivativeCFD50", derivativeCFD50, &b_derivativeCFD50);
  fChain->SetBranchAddress("derivativeCFD60", derivativeCFD60, &b_derivativeCFD60);
  fChain->SetBranchAddress("derivativeCFD70", derivativeCFD70, &b_derivativeCFD70);
  fChain->SetBranchAddress("derivativeCFD80", derivativeCFD80, &b_derivativeCFD80);
  fChain->SetBranchAddress("derivativeCFD90", derivativeCFD90, &b_derivativeCFD90);
  // fChain->SetBranchAddress("derivativeZCD", derivativeZCD, &b_derivativeZCD);
  // fChain->SetBranchAddress("derivativeCTD", derivativeCTD, &b_derivativeCTD);
  fChain->SetBranchAddress("width", width, &b_width);
  fChain->SetBranchAddress("TOT", TOT, &b_TOT);

  Notify();
}

Bool_t GetHist::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void GetHist::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}

Int_t GetHist::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}

#endif // #ifdef GetHist_cxx
